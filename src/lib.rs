// Copyright (c) 2021 Yuri6037
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

use std::io::Read;
use std::io::Result;

const BUF_SIZE: usize = 8192;

pub struct BufferedReader<TReader: Read>
{
    cursor: usize,
    cur_size: usize,
    reader: TReader,
    buffer: [u8; BUF_SIZE]
}

impl <TReader : Read> BufferedReader<TReader>
{
    pub fn new(reader: TReader) -> BufferedReader<TReader>
    {
        return BufferedReader
        {
            cursor: 0,
            cur_size: 0,
            reader: reader,
            buffer: [0; BUF_SIZE]
        }
    }
}

impl <TReader: Read> Read for BufferedReader<TReader>
{
    fn read(&mut self, buf: &mut [u8]) -> Result<usize>
    {
        for i in 0..buf.len()
        {
            if self.cursor >= self.cur_size
            {
                self.cur_size = self.reader.read(&mut self.buffer)?;
                if self.cur_size == 0
                {
                    return Ok(i); //Not all bytes could be read return
                }
                self.cursor = 0;
            }
            buf[i] = self.buffer[self.cursor];
            self.cursor += 1;
        }
        return Ok(buf.len()); //All bytes have been read
    }
}

#[cfg(test)]
mod tests
{
    use std::fs::File;
    use super::*;

    #[test]
    pub fn basic()
    {
        let fle = File::open("./winetricks").unwrap();
        let mut reader = BufferedReader::new(fle);
        let mut dummy: [u8; 4] = [0; 4];
        let res = reader.read(&mut dummy).unwrap();
        assert_eq!(res, 4);
    }

    #[test]
    pub fn large_buffer()
    {
        let fle = File::open("./winetricks").unwrap();
        let mut reader = BufferedReader::new(fle);
        let mut dummy: [u8; 32768] = [0; 32768];
        let res = reader.read(&mut dummy).unwrap();
        assert_eq!(res, 32768);
    }

    #[test]
    pub fn eof()
    {
        let fle = File::open("./winetricks").unwrap();
        let mut reader = BufferedReader::new(fle);
        let mut dummy: [u8; 32768] = [0; 32768];
        let mut res = reader.read(&mut dummy).unwrap();
        while res > 0
        {
            res = reader.read(&mut dummy).unwrap();
            if res < 32768
            {
                //In here the there should no more data
                res = reader.read(&mut dummy).unwrap();
                assert_eq!(res, 0);
                res = reader.read(&mut dummy).unwrap();
                assert_eq!(res, 0);        
            }
            else
            {
                assert_eq!(res, 32768);
            }
        }
    }
}